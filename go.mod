module tp-2023-db1

go 1.19

require github.com/lib/pq v1.10.9

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	golang.org/x/sys v0.14.0 // indirect
)

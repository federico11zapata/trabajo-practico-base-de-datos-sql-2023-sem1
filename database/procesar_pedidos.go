package database

import(
	"database/sql"
//	"fmt"
	"log"
	"strings"
	_"github.com/lib/pq"
)

func Procesar(){
	connStr := "user=postgres host=localhost dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable"
	db, err := sql.Open("postgres", connStr)
	if err != nil {
			log.Fatal(err)
	}
	defer db.Close()
	
	//Recupera todos los registros de entrada_trx_pedidos
	rows, err := db.Query("select * from entrada_trx_pedido")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	
	for rows.Next(){
		var idOrden int
		var operacion string
		var idUsuarie sql.NullInt64
		var idDirEntrega sql.NullInt64
		var idPedido sql.NullInt64
		var idProducto sql.NullInt64
		var cantidad sql.NullInt64
		var fechaHoraEntrega sql.NullString
		
		err := rows.Scan (&idOrden, &operacion, &idUsuarie, &idDirEntrega, &idPedido, &idProducto, &cantidad, &fechaHoraEntrega)
		if err != nil {
			log.Fatal(err)
		}
		
		//Llama a la funcion correspondiente segun la operacion
		operacion = strings.TrimSpace(operacion)
		switch operacion {
		case "creación":
			if idUsuarie.Valid && idDirEntrega.Valid {
				//Llama a la funcion valida_pedido antes de llamar a creacion_del_pedido
				valido, err := validarPedido (db, int(idUsuarie.Int64), int(idDirEntrega.Int64)) //se cambia ya que se espera un int
				if err != nil {
					log.Printf("Error al validar el pedido: %v", err)
					break //¿se debe cortar?
				}
				if valido{
					_, err := db.Exec("select creacion_del_pedido($1,$2)", idUsuarie.Int64, idDirEntrega.Int64)
					if err != nil{
						log.Printf("Error al llamar a la funcion de creacion: %v", err)
					} else {
						log.Printf("Creacion completada.")
					}
				}else{
					log.Println("Pedido no valido, no se llama a la funcion de creacion.")
				}
			} else{
				log.Println("mensaje pendiente...")
			}
		case "producto":
			if idPedido.Valid && idProducto.Valid && cantidad.Valid {
				//Llama a la funcion agregarProducto antes de realizar alguna accion
				agregadoExitoso, err := agregarProducto(db, int(idPedido.Int64), int(idProducto.Int64), int(cantidad.Int64))
				if err != nil {
					log.Printf("Error al procesar el agregado del producto: %v", err)
					break
				}
				
				if agregadoExitoso {
						log.Println("Agregado del producto exitosa.")
				}else{
					log.Println("No se pudo realizar el agregado del producto.")
				}
			} else {
				log.Println()
			}
		case "cierre":
			if idPedido.Valid && fechaHoraEntrega.Valid {
				//Llama a la funcion cerrar_pedido antes de realizar alguna accion
				cierreExitoso, err := cerrarPedido(db, int(idPedido.Int64), fechaHoraEntrega.String)
				
				if err != nil {
					log.Printf("Error al cerrar el pedido: %v", err)
					break
				}
				
				if cierreExitoso {
						log.Println("Cierre del pedido exitoso.")
				}else{
					log.Println("No se pudo cerrar el pedido.")
				}
			} else {
				log.Println()
			}
		case "cancelación":
			if idPedido.Valid {

				//Llama a la funcion cancelarPedido antes de realizar alguna accion
				cancelacionExitosa, err :=cancelarPedido(db, int(idPedido.Int64))
				if err != nil {
					log.Printf("Error al procesar la cancelacion del pedido: %v", err)
					break
				}
				
				if cancelacionExitosa {
					log.Println("Cancelacion del pedido exitosa.")
				} else {
					log.Println("No se pudo realizar el pedido.")
				}
			} else {
				log.Println()
			}

		case "entrega":
			if idPedido.Valid {
				//Llama a la funcion entrega_pedido antes de realizar alguna accion
				entregaExitosa, err := entregaPedido(db, int(idPedido.Int64))
				if err != nil {
					log.Printf("Error al procesar la entrega del pedido: %v", err)
					break
				}
				
				if entregaExitosa {
					log.Println("Entrega del pedido exitosa.")
				} else {
					log.Println("No se pudo realizar la entrega del pedido.")
				}
			} else {
				log.Println()
			}
			
		default:
			log.Printf("Operacion no valida: %s", operacion)
		}
	}
	
	if err:= rows.Err(); err != nil {
		log.Fatal(err)
	}
}

//Funcion para validar el pedido antes de la creacion
func validarPedido(db *sql.DB, idUsuarie, idDirEntrega int) (bool, error) {
	var valido bool
	err := db.QueryRow("select validar_pedido($1,$2)", idUsuarie, idDirEntrega).Scan(&valido) 
	if err != nil {
		return false, err
	}
	
	return valido, nil
}

//Funcion para agregar un producto a un pedido
func agregarProducto(db *sql.DB, idPedido, idProducto, cantidad int) (bool, error){
	var agregadoExitoso bool
	err := db.QueryRow ("select agregar_producto($1, $2, $3)", idPedido, idProducto, cantidad).Scan(&agregadoExitoso)
	if err != nil {
		return false, err
	}
	
	return agregadoExitoso, nil
}

//Funcion para cerrar un pedido
func cerrarPedido(db *sql.DB, idPedido int, fechaEntrega string) (bool, error){
	var cierreExitoso bool
	err := db.QueryRow ("select cerrar_pedido($1, $2)", idPedido, fechaEntrega).Scan(&cierreExitoso)
	if err != nil {
		return false, err
	}
	
	return cierreExitoso, nil
}

//Funcion para procesar la entrega del pedido
func entregaPedido(db *sql.DB, idPedido int)(bool, error){
	var entregaExitosa bool
	err := db.QueryRow("select entrega_de_pedido($1)", idPedido).Scan(&entregaExitosa)
	if err != nil {
		return false, err
	}
	
	return entregaExitosa, nil
}

//Funcion para procesar la cancelacion del pedido
func cancelarPedido(db *sql.DB, idPedido int) (bool, error) {
	var cancelacionExitosa bool
	err := db.QueryRow("select cancelarPedido($1)", idPedido).Scan(&cancelacionExitosa)
	if err != nil {
		return false, err
	}
	return cancelacionExitosa, nil
}

// Función para verificar si la reposición es necesaria
func verificarReposicion(db *sql.DB, idProducto int) (bool, error) {
    // Consulta para obtener la información del producto, incluido el stock disponible y reservado
    query := `
        SELECT stock_disponible, stock_reservado, punto_reposicion
        FROM producto
        WHERE id_producto = $1
    `
    var stockDisponible, stockReservado, puntoReposicion int
    err := db.QueryRow(query, idProducto).Scan(&stockDisponible, &stockReservado, &puntoReposicion)
    if err != nil {
        return false, err
    }

    // Verificar si la reposición es necesaria
    return (stockDisponible+stockReservado) < puntoReposicion, nil
}
// Función para realizar la reposición




//es problable que este alargando de mas con entregaPedido, cerrarPedido y incorporacionPedido, ya que en realidad en estos casos las 

//validaciones estan incorporadas dentro de las funciones a diferencia de validarPedido que si tenia una funcion para validar aparte
//la funcion validarPedido en sql la usa creacion_pedido primero se valida el pedido y despues se llama para crear el pedido

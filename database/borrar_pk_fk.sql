--primero se deben borrar las FK
--FK de la tabla rireccion entrega
alter table direccion_entrega drop constraint direccion_entrega_id_usuarie_fk;
--FK de la tabla pedido detalle
alter table pedido_detalle drop constraint pedido_detalle_id_pedido_fk;
--FK de la tabla reposicion
alter table reposicion drop constraint reposicion_id_producto_fk;

--luego se borran las PK
		--PK de tabla cliente
		alter table cliente drop constraint cliente_pk;
		
		--PK de tabla direccion entrega
		alter table direccion_entrega drop constraint direccion_entrega_pk;
							
		--PK de la tabla tarifa entrega
		alter table tarifa_entrega drop constraint tarifa_entrega_pk;
										
		--PK de tabla producto 
		alter table producto drop constraint producto_pk;
		
		-- PK de la tabla pedido
		alter table pedido drop constraint pedido_pk;
		
		--PK pedido detalle
		alter table pedido_detalle drop constraint pedido_detalle_pk;
		
		--PK reposicion
		alter table reposicion drop constraint reposicion_pk;
		
		--PK error
		alter table error drop constraint error_pk;
		
		--PK envio email
		alter table envio_email drop constraint envio_mail_pk;	

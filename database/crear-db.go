// no se olviden de hacer:
//
// $ gofmt -w nombre-archivo.go
//
// cada vez antes de hacer el commit

package database // los nombres de paquetes van en minúsculas

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func createDataBase() error {
	db, err := sql.Open(`postgres`, `user=postgres host=localhost 
	dbname=postgres sslmode=disable`)
	if err != nil {
		return err
	}
	defer db.Close()

	_, err = db.Exec(`drop database IF EXISTS albarracin_bravo_gonzalez_zapata_db1;`)
	if err != nil {
		return err
	}

	_, err = db.Exec(`create database albarracin_bravo_gonzalez_zapata_db1`)
	if err != nil {
		return err
	}

	return nil
}

func Crear() {
	if err := createDataBase(); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Base de datos creada\n")
}

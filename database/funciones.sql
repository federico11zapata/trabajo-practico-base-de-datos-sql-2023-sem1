-- ## seccion 2
-- logica de creacion del pedido
-- validar pedido

create or replace function validar_pedido (id_us int, id_dir_entregas int) returns boolean as $$
declare
	validez_a boolean;
	validez_b boolean;
	validez_c boolean;
	validez_res boolean;
	cod_postal_c char(4);
	res_direccion record;
begin
	perform nombre from cliente where id_usuarie=id_us;
	if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'creación',current_timestamp,'?id de usuarie no válido');
		validez_a := false;
	else
		validez_a :=true;
	end if;
	select * into res_direccion from direccion_entrega where id_usuarie = id_us and id_direccion_entrega = id_dir_entregas;
	 if not found then 
		 insert into error values (default,null,null,null,null,null,null,null,null,null,'creación',current_timestamp,'?id de dirección no válido');
		 validez_b := false;
	 else
		 validez_b := true;
		 cod_postal_c:= substring(res_direccion.codigo_postal::text from 2 for 4);
	 end if;
	
	 perform costo from tarifa_entrega where codigo_postal_corto=cod_postal_c;
	 if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'creación',current_timestamp,'?dirección de entrega fuera del área de atención');
		 validez_c := false;
	 else
		 validez_c := true;
	 end if;
	validez_res := validez_a and validez_b and validez_c;
	return validez_res;
end;
$$ language plpgsql;
-- fin sec 2


-- ## seccion 3 
-- ingresar pedido
create or replace function creacion_del_pedido(id_us int, id_dir_entregas int) returns boolean as $$
declare
	resultado boolean;
	res_direccion record;
	tarifa record;
	cod_postal_c char(4);
	costo_t decimal(12,2);
begin
	resultado:=validar_pedido(id_us,id_dir_entregas);
	if resultado = true then
		select * into res_direccion from direccion_entrega where id_usuarie=id_us;
		cod_postal_c:= substring(res_direccion.codigo_postal::text from 2 for 4);
		select * into tarifa from tarifa_entrega where codigo_postal_corto=cod_postal_c; 
		costo_t:=tarifa.costo;
		insert into pedido values (default, current_timestamp, null, null, null, id_us, id_dir_entregas, 0, costo_t, 'ingresado');
		return true;
	else  
		return false;
	end if;
end;
$$ language plpgsql;
--


-- ## seccion 4 
-- logica de entrega del pedido
-- validar pedido entregado
create or replace function validar_pedido_completado (id_ped int) returns boolean as $$
	declare
		ped record;
		validez_a boolean;
		validez_b boolean;
		validez_res boolean;
		estado_p char(10);
	begin
		perform id_usuarie from pedido where id_pedido=id_ped;
		if not found then
			insert into error values (default,null,null,null,null,null,null,null,null,null,'entrega',current_timestamp,'?id de pedido no válido');
			validez_a:=false;
		else
			validez_a:=true;
		end if;
		select * into ped from pedido where id_pedido = id_ped;
		estado_p := ped.estado;
		if estado_p='completado' then
			validez_b:=true;
		else
			insert into error values (default,null,null,null,null,null,null,null,null,null,'entrega',current_timestamp,'?pedido sin cerrar o ya entregado');
			validez_b:=false;
		end if;
		validez_res:=validez_a and validez_b;
		return validez_res;
	end;
$$ language plpgsql;
-- 


-- ## seccion 5 
-- entrega del pedido
create or replace function entrega_de_pedido (id_ped int) returns boolean as $$
declare
	pedido_e record;
	detalle_e record;
	cliente_e record;
	direccion_e record;
	email_e text;
	id_cliente_e int;
begin
	if validar_pedido_completado(id_ped) = true then
		update pedido set estado = 'entregado' where id_pedido = id_ped;
		--busco el id del usuario
		select * into pedido_e from pedido where id_pedido=id_ped;
		id_cliente_e := pedido_e.id_usuarie;
		--con el id del usuario que encontre busco el email
		select * into cliente_e from cliente where id_usuarie=id_cliente_e; 
		email_e := cliente_e.email;
		--guardo los datos de la direccion y del detalle
		select * into direccion_e from direccion_entrega where id_usuarie=id_cliente_e;
		select * into detalle_e from pedido_detalle where id_pedido=id_ped;
		insert into envio_email values (5, current_timestamp, email_e, 'Pedido entregado', 'Fecha del pedido: '|| pedido_e.f_pedido::text || ' 
		,Fecha de entrega: '||pedido_e.fecha_entrega::text || ' ,Direccion de entrega: '||direccion_e.direccion||' '||direccion_e.localidad||' '||direccion_e.codigo_postal||'
		,Monto total: '||pedido_e.monto_total::text||', Costo de envío: '||pedido_e.costo_envio::text||', Cantidad de productos: '||detalle_e.cantidad::text||' ,Precio unitario: '||detalle_e.precio_unitario::text,current_timestamp, 'enviado'); 
		return true;
	else
		return false;
	end if;
end;
$$ language plpgsql;


-- ## seccion 6 
-- funcion que ejecuta el trigger para restar el stock
create or replace function update_estado_entrega() returns trigger as $$
declare
	ped record;
	ped_detalle record;
	cant int;
	id_prod int;
	id_ped int;
begin
		select * into ped from pedido where id_pedido=new.id_pedido;
		id_ped:=ped.id_pedido;
		select * into ped_detalle from pedido_detalle where id_pedido=id_ped;
		cant := ped_detalle.cantidad;
		id_prod := ped_detalle.id_producto;
		update producto set stock_reservado=stock_reservado-cant where id_producto=id_prod;
		return new;
end;
$$ language plpgsql;


-- ## seccion 7 
-- creacion del trigger para update 'entrega'
create or replace trigger update_estado_entrega_tgr
	after update of estado on pedido
	for each row
	when (new.estado like 'entregado')
	execute procedure update_estado_entrega();


-- ## seccion 8
-- Agregar producto
create or replace function agregar_producto(id_ped int, id_prod int, cant int) returns boolean as $$
declare
	precio_unitario_prod decimal(12,2);
	cantidad_calculada int;
	detalle record;
begin
	-- que id_pedido exista
	perform * from pedido where id_pedido = id_ped;
	if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'producto',null,'?id de pedido no válido');
		return false;
	end if;

	-- que el pedido se encuentre en estado 'ingresado'
	perform * from pedido where id_pedido = id_ped and estado = 'ingresado';
	if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'producto',null,'?pedido cerrado');
		return false;
	end if;

	-- que el id_producto exista
	perform * from producto where id_producto = id_prod;
	if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'producto',null,'?id de producto no válido');
		return false;
	end if;

	-- que el producto tenga stock disponible
	perform * from producto where id_producto = id_prod and stock_disponible >= cant;
	if not found then
		insert into error values (default,null,null,null,null,null,null,null,null,null,'producto',null,'?stock no disponible para el producto ' + id_prod);
		return false;
	end if;
	
	-- incorporar pedido a la tabla detalle
	select precio_unitario into precio_unitario_prod from producto where id_producto = id_prod;
	select * into detalle from pedido_detalle where id_pedido = id_ped and id_producto = id_prod;
	if not found then
		insert into pedido_detalle values (id_ped, id_prod, cant, precio_unitario_prod);
		perform actualizar_stock(id_prod, cant);
		perform actualizar_monto_total(id_ped, cant, precio_unitario_prod);
	else
		-- en caso de que el pedido ya contenga el producto
		-- se le suma la nueva cantidad solicitada, a la cantidad ya registrada
		cantidad_calculada := detalle.cantidad + cant;
		update pedido_detalle set cantidad = cantidad_calculada where id_pedido = id_ped and id_producto = id_prod;
		perform actualizar_stock(id_prod, cant);
		perform actualizar_monto_total(id_ped, cant, precio_unitario_prod);
	end if;

	return true;
end;
$$ language plpgsql;


-- ## seccion 8.1
-- funcion que actualiza el stock disponible y reservado de un producto
create or replace function actualizar_stock(id_prod int, cant int) returns void as $$
declare
	stock_disponible_actual int;
	stock_reservado_actual int;
	stock_disponible_calculado int;
	stock_reservado_calculado int;
begin
	-- actualizacion stock disponible
	select stock_disponible into stock_disponible_actual from producto where id_producto = id_prod;
	stock_disponible_calculado := stock_disponible_actual - cant;
	update producto set stock_disponible = stock_disponible_calculado where id_producto = id_prod;
	
	-- actualizacion stock reservado
	select stock_reservado into stock_reservado_actual from producto where id_producto = id_prod;
	stock_reservado_calculado := stock_reservado_actual + cant;
	update producto set stock_reservado = stock_reservado_calculado where id_producto = id_prod;
end;
$$ language plpgsql;


-- ## seccion 9
-- Funcion que actualiza el monto total de un pedido teniendo en cuenta el detalle
create or replace function actualizar_monto_total(id_ped int, cantidad int, precio_unitario decimal(12,2)) returns void as $$
declare
	monto_total_calculado decimal(12,2);
	monto_total_actual decimal(12,2);
begin
	-- actualizacion monto total
	select monto_total into monto_total_actual from pedido where id_pedido = id_ped;
	monto_total_calculado := monto_total_actual + (precio_unitario * cantidad);
	update pedido set monto_total = monto_total_calculado where id_pedido = id_ped;
end;
$$ language plpgsql;



-- ## seccion 11
-- Cierre de pedido
create or replace function cerrar_pedido(pedido_id int, fecha_entrega timestamp) returns boolean as $$ 
declare
	pedido_valido 	boolean;
	tiene_productos boolean;
	fecha_valida 	boolean;
	motivo_error 	varchar(64);
	cliente_email 	text;
	pedido_info 	text;
begin
	--Validar si el pedido existe
	select exists(select 1 from pedido where id_pedido = pedido_id) into pedido_valido;
	
	--Validar si el pedido tiene al menos un producto agregado
	select exists(select 1 from pedido_detalle where id_pedido = pedido_id) into tiene_productos;
	
	--Validar si la fecha de entrega es psoterior a la fecha actual
	fecha_valida := (fecha_entrega > current_timestamp);
	
	--Realiza validaciones y acciones en funcion de los resultados
	if not pedido_valido then
		motivo_error := '?id de pedido no valido';
	elseif not tiene_productos then
		motivo_error := '?pedido vacio';
	elseif not fecha_valida then
		motivo_error := '?fecha de entrega no valida';
	else
		--Obtener la direccion de correo electronico del cliente
		select email into cliente_email from cliente 
		where id_usuarie = (select id_usuarie from pedido where id_pedido = pedido_id);
		
		--Obtener la informacion del pedido
		select concat('Pedido: ', id_pedido, ', Monto Total: ', monto_total, ', Costo de Envio: ', costo_envio)
		into pedido_info from pedido where id_pedido = pedido_id;
		
		--Calcular la 'hora hasta' como dos horas posterior a la 'hora desde'
		update pedido
		set
			fecha_entrega = cerrar_pedido.fecha_entrega,
			hora_entrega_desde = current_time,
			hora_entrega_hasta = current_time + interval '2 hours',
			estado = 'completado'
		where id_pedido = pedido_id;
		
		--Insertar en la tabla envio_email
		insert into envio_email (
			id_email, f_generacion, email_cliente, asunto,
			cuerpo, f_envio, estado
		) values (
			default, current_timestamp, cliente_email,'Pedido aceptado', 
			pedido_info, null, 'Pendiente'
		);
		
		return true;
	end if;

	--Registrar el error
	insert into error (
		id_error, id_pedido, f_pedido, id_usuarie, id_direccion_entrega,
		direccion, localidad, codigo_postal, id_producto, cantidad,
		operacion, f_error, motivo
	) values (
		default, pedido_id, current_timestamp, null, null,
		null, null, null, null, null,
		'cierre', current_timestamp, motivo_error
	);
	
	return false;
end;
$$ language plpgsql;



-- ## seccion 12
-- Cancelacion de pedido
create or replace function cancelarPedido(p_id_pedido int) returns boolean as $$


begin
    -- Verificamos que el id de pedido exista
    if not exists (select id_pedido from pedido where id_pedido = p_id_pedido) then
        -- Si el id de pedido no existe, lanzamos un error
        insert into error (id_error,id_pedido, f_pedido, id_usuarie, id_direccion_entrega, direccion, localidad, codigo_postal, id_producto, cantidad, operacion, f_error, motivo)
        values (default,p_id_pedido, null, null, null, null, null, null, null, null, 'cancelacion', current_timestamp, 'id no valido.');
        -- Retornamos false indicando que la cancelación no fue exitosa
        raise notice 'Error: id de pedido % no válido.', p_id_pedido;
        return false;
    end if;

    --verificamos que el pedido este marcado como ingresado o completado
    if not exists (select 1 from pedido where id_pedido = p_id_pedido and estado in('ingresado','completado')) then
    
        -- Si el pedido no cumple con alguno de esos dos estados, lanzamos un error
        insert into error (id_error,id_pedido, f_pedido, id_usuarie, id_direccion_entrega, direccion, localidad, codigo_postal, id_producto, cantidad, operacion, f_error, motivo)
        values (default,p_id_pedido, null, null, null, null, null, null, null, null, 'cancelacion', current_timestamp, 'Error: el pedido ya fue cancelado o entregado.');
        -- Retornamos false indicando que la cancelación no fue exitosa
        raise notice'Error: pedido % ya fue entregado o cancelado.', p_id_pedido;
		return false;
    end if;
    
    -- si pasamos todas las validaciones, entonces comenzamos con la cancelacion del pedido
    --seteamos el estado del pedido como cancelado

    --aislamos esta parte del codigo que se encarga de hacer el update y modificar la db
	begin
		update pedido set estado = 'cancelado' where id_pedido = p_id_pedido;
		--renovamos el stock y lo sacamos del stock reservado
		update producto set stock_disponible = stock_disponible + cantidad, stock_reservado = stock_reservado - pd.cantidad
		from pedido_detalle pd where pd.id_pedido = p_id_pedido and pd.id_producto = producto.id_producto;
		--retornamos true al lograr cancelar el pedido
		return true;
	commit;
	end;
end;
$$ language plpgsql;



-- ## seccion 13
create or replace function solicitudDeReposicion() returns boolean as $$

	declare
		fecha_actual date;
	begin
	--primero declaramos la fecha actual
		fecha_actual := current_date;
		
	--verificamos si ya existe una solicitud de reposicion para esta fecha
	if exists (select 1 from reposicion where fecha_solicitud = fecha_actual and estado='pendiente') then
		--si ya existe devolvemos false
		return false;
	else
	--insertamos en la tabla reposicion los siguientes datos
		insert into reposicion(id_producto, fecha_solicitud, cantidad_a_reponer, estado) 
		--los datos que nos trae la siguiente tabla...usamos greatest para asegurarnos de que no se ponga un numero negativo
			select id_producto, current_date, (greatest(stock_maximo - (stock_disponible+stock_reservado),0)),'pendiente'
				--filtramos solo los productos que esten en su punto de reposicion(segun la condicion del enunciado)
				from producto where stock_disponible + stock_reservado <= punto_reposicion;
				return true;
	end if;
end;
$$ language plpgsql;


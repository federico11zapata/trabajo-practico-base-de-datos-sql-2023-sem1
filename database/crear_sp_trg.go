package database

import (
	"bufio"
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"
	//"io/ioutil"
	_ "github.com/lib/pq"
	)

//Ruta del archivo de creacion de SP y TRG
const funcionesSQLPath = "database/funciones.sql"
const delimitador = "-- ##"		//se agrego debido a la dificultad de identificar que parte del codigo ocurria algun error al cargarse

func procesarSeccion(db * sql.DB, seccion string, seccionID int) error {
	_, err := db.Exec(seccion)
	if err != nil {
		return fmt.Errorf("error al procesar la seccion %d: %v", seccionID, err)
	}
	return nil
}

func createFuncionesFromFile(db * sql.DB, filePath string) error {
	archivo, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("error al abrir el archivo de funciones: %v", err)
	}
	defer archivo.Close()
	
	scanner := bufio.NewScanner(archivo)
	var seccion string
	seccionID := 1
	
	for scanner.Scan() {
		linea := scanner.Text()
		
		if strings.HasPrefix(linea, delimitador){
			//Procesa la seccion anterior si existe
			if seccion != ""{
				if err := procesarSeccion(db, seccion, seccionID); err != nil {
					return fmt.Errorf("error al procesar la seccion: %v", err)
				}
				seccionID++
			}
		
			//comienza una nueva seccion
			seccion = ""
		} else {
			//agregar la linea a la seccion actual
			seccion += linea + "\n"
		}
	}
	
	//procesa la ultima seccion si existe
	if seccion != "" {
		if err := procesarSeccion(db, seccion, seccionID); err != nil {
			return fmt.Errorf ("error al procesar la seccion: %v", err)
		}
	}
	
	return scanner.Err()
}
	
func crearFunciones(db *sql.DB, filePath string) error {
	fmt.Printf("Creando Procedures & Triggers...\n")
	if err:= createFuncionesFromFile(db, filePath); err!=nil{
		log.Fatal(err)
		}
		return nil
	}

func AgregarFunciones(){
	db, err := obtenerConexion()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	//Ruta del archivo de funciones
	err = crearFunciones(db, funcionesSQLPath)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Procedures & Triggers creados\n")
}
		

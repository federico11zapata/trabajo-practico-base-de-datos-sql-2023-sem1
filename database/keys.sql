--Agregar PK's
		--PK de tabla cliente
		alter table cliente add constraint cliente_pk primary key (id_usuarie);
		
		--PK de tabla direccion entrega
		alter table direccion_entrega add constraint direccion_entrega_pk primary key (id_usuarie, id_direccion_entrega);
							
		--PK de la tabla tarifa entrega
		alter table tarifa_entrega add constraint tarifa_entrega_pk primary key (codigo_postal_corto);
										
		--PK de tabla producto 
		alter table producto add constraint producto_pk primary key (id_producto);
		
		-- PK de la tabla pedido
		alter table pedido add constraint pedido_pk primary key (id_pedido);
		
		--PK pedido detalle
		alter table pedido_detalle add constraint pedido_detalle_pk primary key (id_pedido, id_producto);
		
		--PK reposicion
		alter table reposicion add constraint reposicion_pk primary key (id_producto, fecha_solicitud);
		
		--PK error
		alter table error add constraint error_pk primary key (id_error);
		
		--PK envio email
		alter table envio_email add constraint envio_mail_pk primary key (id_email);	

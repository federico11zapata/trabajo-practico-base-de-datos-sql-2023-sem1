package database 

import (
	"database/sql"
	"fmt"
	"log"
	"io/ioutil"
	_ "github.com/lib/pq"
)

//Ruta del archivo de claves primarias
const keysSQLPath = "database/keys.sql"
const dropKeysSQLPath = "database/borrar_pk_fk.sql"

//createPKFromFile crea claves primarias desde un archivo SQL
func createPKFromFile(db * sql.DB, filePath string) error {
	// Leer el archivo de claves primarias	
	schema, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("error al leer el archivo de claves primarias: %v", err)
	}
	
	//Ejecutar las consultas SQL desde el archivo
	_, err = db.Exec(string(schema))
	if err != nil {
			return fmt.Errorf("error al ejecutar las claves primarias desde el archivo %s: %v", filePath, err)
	}	
	
	return nil
}

//dropPKFKromFile ...
func dropPKFKromFile(db * sql.DB, filePath string) error {
	// Leer el archivo de claves primarias	
	schema, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("error al leer el archivo de borrado: %v", err)
	}
	
	//Ejecutar las consultas SQL desde el archivo
	_, err = db.Exec(string(schema))
	if err != nil {
			return fmt.Errorf("error al ejecutar las claves primarias: %v", err)
	}	
	
	return nil
}

//agregarPK asigna claves primarias desde un archivo SQL
func agregarPK(db *sql.DB, filePath string) error {
	fmt.Printf("asignando Primary Keys...\n")

	if err := createPKFromFile(db, filePath); err != nil {
		 return fmt.Errorf("Error al asignar Primary Keys: %v", err)
	}
	return nil
}

//agregarFK asigna claves foraneas directamente en el codigo SQL
func agregarFK(db *sql.DB) error {
	fmt.Printf("asignando Foreign Keys...\n")
	_, err := db.Exec(`
		--FK direccion entrega
		alter table direccion_entrega add constraint direccion_entrega_id_usuarie_fk 
		foreign key (id_usuarie) references cliente (id_usuarie);
		
		--FK pedido_detalle
		alter table pedido_detalle add constraint pedido_detalle_id_pedido_fk 
		foreign key (id_pedido) references pedido (id_pedido);
		
		--FK reposicion
		alter table reposicion add constraint reposicion_id_producto_fk 
		foreign key (id_producto) references producto (id_producto);
	`)

	return err
}

//borrar elimina claves primarias y foraneas desde un archivo SQL
func borrar(db *sql.DB, filePath string) error {
	fmt.Printf("borrando Primary Keys & Foreign Keys...\n")

	if err := dropPKFKromFile(db, filePath); err != nil {
		return fmt.Errorf("Error al borrar Primary Kes y Foreign keys: %v", err)
	}
	return nil
}

//AsignarPKFK reliza la asignacion de claves primarias y foraneas
func AsignarPKFK() {
	db, err := obtenerConexion()
	if err != nil {
		log.Fatalf("Error al obtener la conexion a la base de datos: %v", err)
	}
	defer db.Close()

	//Ruta del archivo de claves primarias
	err = agregarPK(db, keysSQLPath)
	if err != nil {
		log.Fatalf("Error al asignar claves primarias: %v",err)
	}

	err = agregarFK(db)
	if err != nil {
		log.Fatalf("Error al asignar claves foraneas: %v", err)
	}

	fmt.Printf("Primary Keys & Foreign Keys asignadas\n")
}

//BorrarPKFK realiza la eliminacion de claves primarias y foraneas
func BorrarPKFK() {
		db, err := obtenerConexion()
	if err != nil {
		log.Fatalf("Error al obtener la conexion a la base de datos: %v", err)
	}
	defer db.Close()

	//Ruta del archivo de claves primarias
	err = borrar(db, dropKeysSQLPath)
	if err != nil {
		log.Fatalf("Error al borrar claves primarias y foraneas: %v", err)
	}

	fmt.Printf("Primary Keys & Foreign Keys borradas\n")
}
	

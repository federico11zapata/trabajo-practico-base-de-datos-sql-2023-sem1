package database

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

func FuncionesManual() {
	//Conexion a la base de datos
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	var opcion int
	var continuar string

	for {
		//Menu
		fmt.Println("1. Crear Pedido")
		fmt.Println("2. Agregar Producto")
		fmt.Println("3. Cerrar Pedido")
		fmt.Println("4. Cancelar Pedido")
		fmt.Println("5. Entregar Pedido")
		fmt.Println("6. Generar Solicitud de Reposicion")
		fmt.Println("7. Volver atras")

		fmt.Println("Ingrese el numero de la opcion deseada: ")
		fmt.Scan(&opcion)

		//Variables para almacenar datos de entrada
		var idUsuario, idDireccion, idPedido, idProducto, cantidad int
		var fechaEntrega, horaEntrega string

		//switch para manejar las opciones
		switch opcion {
		case 1:
			fmt.Println("Opcion seleccionada: Crear Pedido")

			//Pedir datos de entrada
			fmt.Print("Ingrese ID de Usuario: ")
			fmt.Scan(&idUsuario)
			fmt.Print("Ingrese ID de Direccion de entrega: ")
			fmt.Scan(&idDireccion)

			//ejecuto la función creacion_de_pedido
			creacionExitosa, err := crearPedido(db, idUsuario, idDireccion)
			if err != nil {
				log.Printf("Error al crear el pedido: %v", err)
				break
			}

			if creacionExitosa {
				log.Println("creación del pedido exitoso.")
			} else {
				log.Println("Error al crear el pedido.")
			}

		case 2:
			fmt.Println("Opcion seleccionada: Agregar Producto")

			//Pedir datos de entrada
			fmt.Print("Ingrese ID de Pedido: ")
			fmt.Scan(&idPedido)
			fmt.Print("Ingrese ID de Producto: ")
			fmt.Scan(&idProducto)
			fmt.Print("Ingrese Cantidad: ")
			fmt.Scan(&cantidad)

			//ejecuto la función agregar_producto
			agregadoExitoso, err := agregadoDelProducto(db, idPedido, idProducto, cantidad)
			if err != nil {
				log.Printf("Error al agregar el producto: %v", err)
				break
			}

			if agregadoExitoso {
				log.Println("Agregado de producto exitoso.")
			} else {
				log.Println("Error al agregar el producto.")
			}

		case 3:
			fmt.Println("Opcion seleccionada: Cerrar Pedido")

			//Pedir datos de entrada
			fmt.Print("Ingrese ID de Pedido: ")
			fmt.Scan(&idPedido)
			fmt.Print("Ingrese Fecha de Entrega: ")
			fmt.Scan(&fechaEntrega)
			fmt.Print("Ingrese Hora de Entrega: ")
			fmt.Scan(&horaEntrega)
			
			//Unir fecha y hora
			fechaHoraEntrega := fmt.Sprintf("%s %s", fechaEntrega, horaEntrega) 
			
			//Convertir la cadena de fecha y hora a un objeto time.Time
			fechaHora, err := time.Parse("2006-01-02 15:04", fechaHoraEntrega)
			if err != nil {
				log.Fatalf("Error al analizar la fecha y hora: %v", err)
			}
			
			cierreExitoso, err := cerrandoPedido(db, idPedido, fechaHora)
			if err != nil {
				log.Println("Error al cerrar el pedido: %v", err)
				break
			}
			
			if cierreExitoso {
				log.Println("Cierre del pedido exitoso.")
			} else {
				log.Println("Error al cerrar el pedido.") //puede que este de mas
			}
		case 4:
			fmt.Println("Opcion seleccionada: Cancelar Pedido")

			// Pedir datos de entrada
			fmt.Print("Ingrese ID de Pedido: ")
			fmt.Scan(&idPedido)

			// Ejecuto la función cancelarPedido
			cancelacionExitosa, err := cancelacionPedido(db, idPedido)
			if err != nil {
				log.Printf("Error en la cancelacion de pedido: %v", err)
				break
			}

			if cancelacionExitosa {
				log.Println("Cancelación del pedido exitosa.")
			} else {
				log.Println("Error al cancelar el pedido.")
			}	

		case 5:
			fmt.Println("Opcion seleccionada: Entregar Pedido")

			//Pedir datos de entrada
			fmt.Print("Ingrese ID de Pedido: ")
			fmt.Scan(&idPedido)

			//Ejecuto la función entrega_de_pedido
			entregaExitosa, err := entregaDelPedido(db, idPedido)
			if err != nil {
				log.Printf("Error en la entrega del pedido: %v", err)
				break
			}
			if entregaExitosa {
				log.Println("Pedido entregado exitosamente.")
			} else {
				log.Println("Error al actualizar el estado del pedido a entregado.")
			}

		case 6:
			fmt.Println("Opcion seleccionada: Generar Solicitud de Reposicion")
			reposicionExitosa, err := realizarReposicion(db)
			if err != nil {
				log.Printf("Error al crear solicitud de reposicion %v", err)
				break
			}

			if reposicionExitosa {
				log.Println("Reposicion del producto exitosa.")
			} else {
				log.Println("Error al generar solicitud de reposicion.")
			}
		case 7:
			//vacio aproposito
		default:
			fmt.Println("Opcion no valida")
		}

		//Preguntar al usuario si desea continuar
		fmt.Print("¿Desea regresar al menu anterior? (s/n):")
		fmt.Scan(&continuar)

		if continuar != "n" {
			break
		}
	}

	fmt.Println("Regresando al menu anterior.")
	return
}

func crearPedido(db *sql.DB, idUsuario int, idDireccion int) (bool, error) {
	var valido bool
	err := db.QueryRow("select creacion_del_pedido($1,$2)", idUsuario, idDireccion).Scan(&valido)
	if err != nil {
		return false, err
	}
	return valido, nil
}

func agregadoDelProducto(db *sql.DB, idPedido, idProducto, cantidad int) (bool, error) {
	var valido bool
	err := db.QueryRow("select agregar_producto($1,$2,$3)", idPedido, idProducto, cantidad).Scan(&valido)
	if err != nil {
		return false, err
	}
	return valido, nil
}

func entregaDelPedido(db *sql.DB, idPedido int) (bool, error) {
	var valido bool
	err := db.QueryRow("select entrega_de_pedido($1)", idPedido).Scan(&valido)
	if err != nil {
		return false, err
	}
	return valido, nil
}

// Función para cancelar un pedido utilizando el stored procedure en la base de datos
func cancelacionPedido(db *sql.DB, idPedido int) (bool, error) {
    var valido bool

    // llamada al procedimiento cancelar pedido
    err := db.QueryRow("select cancelarPedido($1)", idPedido).Scan(&valido)
    if err != nil {
        return false, err
    }

    // Retornar el resultado de la cancelación y cualquier posible error
    return valido, nil
}

func cerrandoPedido(db *sql.DB, idPedido int, fechaHora time.Time) (bool, error) {
	var valido bool
	
	//Llama al procedimiento almacenado cerrar_pedido
	err := db.QueryRow("select cerrar_pedido($1,$2)", idPedido, fechaHora).Scan(&valido)
	if err != nil {
		return false, err
	}
	
	//Retorna el resultado del cierre y cualquier posible error
	return valido, nil
}
func realizarReposicion(db *sql.DB) (bool, error) {
    // Iniciar una transacción
    tx, err := db.Begin()
    if err != nil {
        return false, err
    }
    defer tx.Rollback()

    // Llamar al nuevo stored procedure de reposición dentro de la transacción
    _, err = tx.Exec("SELECT solicitudDeReposicion()")
    if err != nil {
        return false, err
    }

    // Commit si todo fue exitoso, de lo contrario, se realizará un rollback
    err = tx.Commit()
    if err != nil {
        return false, err
    }

    return true, nil
}

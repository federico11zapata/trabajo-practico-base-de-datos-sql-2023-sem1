package database

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"time"
	_ "github.com/lib/pq"
)

//Conexion es la cadena necesaria para conectar a la base de datos
const conexion = "user=postgres host=localhost dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable"

//Ruta del archivo de esquema SQL
const esquemaSQLPath = "database/esquema.sql"

func obtenerConexion() (*sql.DB, error) {
	db, err := sql.Open("postgres", conexion)
	if err != nil {
		return nil, err
	}
	return db, nil
}

//createTablesFromFile crea tablas en la base de datos desde un archivo SQL de esquema
func createTablesFromFile(db * sql.DB, filePath string) error {
	// Leer el archivo de esquema.sql
	schema, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("error al leer el archivo de esquema: %v", err)
	}
	
	//Ejecutar las consultas SQL desde el archivo
	_, err = db.Exec(string(schema))
	if err != nil {
			return fmt.Errorf("error al ejecutar el esquema desde el archivo %s: %v", filePath, err)
	}	
	
	return nil
}

//CrearTablas ...
func CrearTablas() { 
	//Obtener conexion a la base de datos
	db, err := obtenerConexion()
	if err != nil {
		log.Fatalf("Error al obtener la conexion a la base de datos: %v", err)
	}
	defer db.Close()
	
	//Crear tablas desde el archivo de esquema
	if err := createTablesFromFile(db, esquemaSQLPath); err != nil {
		log.Fatalf("Error al crear tablas desde el archivo de esquema: %v", err)
	}
	
	time.Sleep(1 * time.Second)
	
	fmt.Printf("Tablas creadas\n")
}

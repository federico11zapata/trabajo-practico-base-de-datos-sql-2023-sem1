package database

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/boltdb/bolt"
	_ "github.com/lib/pq"
)

//-----------Creacion de la DB, buckets y escritura de datos---------------------
func EscribirClientesBOLT() {
	conSTR := "user=postgres dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable";
	// Abrir la conexión a la base de datos SQL
	dbSQL, err := sql.Open("postgres", conSTR )
	if err != nil {
		log.Fatal(err)
	}
	defer dbSQL.Close()

	// Abrir la base de datos BoltDB 
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	//consulta SQL para obtener datos de clientes
	rows, err := dbSQL.Query("SELECT * FROM cliente")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Actualizar BoltDB con datos de clientes
	err = dbBolt.Update(func(tx *bolt.Tx) error {
		// Crear o abrir el bucket "cliente"
		bucket, err := tx.CreateBucketIfNotExists([]byte("cliente"))
		if err != nil {
			return err
		}

		// procesar resultados que vienen de la consulta sql
		for rows.Next() {
			var id_usuarie int
			var nombre, apellido string
			var dni int
			var fechaNacimiento string
			var telefono, email string

			// Escanear los valores desde la fila SQL
			err := rows.Scan(&id_usuarie, &nombre, &apellido, &dni, &fechaNacimiento, &telefono, &email)
			if err != nil {
				log.Fatal(err)
			}

			// Formatear los datos según sea necesario antes de insertar en BoltDB
			datosCliente := fmt.Sprintf("%d,%s,%s,%d,%s,%s,%s", id_usuarie, nombre, apellido, dni, fechaNacimiento, telefono, email)

			// Insertar datos en el bucket usando el ID del cliente como clave
			err = bucket.Put([]byte(fmt.Sprintf("%d", id_usuarie)), []byte(datosCliente))
			if err != nil {
				return err
			}
			
		}
		fmt.Println("insertados clientes en boltdb")
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}
func EscribirProductosBOLT(){
	// Abrir la conexión a la base de datos SQL
	dbSQL, err := sql.Open("postgres", "user=postgres dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer dbSQL.Close()

	// Abrir la base de datos BoltDB en modo lectura y escritura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar consulta SQL para obtener datos de productos
	rows, err := dbSQL.Query("SELECT * FROM producto")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Actualizar BoltDB con datos de productos
	err = dbBolt.Update(func(tx *bolt.Tx) error {
		// Crear o abrir el bucket "producto"
		bucket, err := tx.CreateBucketIfNotExists([]byte("producto"))
		if err != nil {
			return err
		}

		// Procesar resultados
		for rows.Next() {
			var idProducto int
			var nombre string
			var precioUnitario float64
			var stockDisponible, stockReservado, puntoReposicion, stockMaximo int

			// Escanear los valores desde la fila SQL
			err := rows.Scan(&idProducto, &nombre, &precioUnitario, &stockDisponible, &stockReservado, &puntoReposicion, &stockMaximo)
			if err != nil {
				log.Fatal(err)
			}

			// Formatear los datos según sea necesario antes de insertar en BoltDB
			datosProducto := fmt.Sprintf("%d,%s,%.2f,%d,%d,%d,%d", idProducto, nombre, precioUnitario, stockDisponible, stockReservado, puntoReposicion, stockMaximo)

			// Insertar datos en el bucket usando el ID del producto como clave
			err = bucket.Put([]byte(fmt.Sprintf("%d", idProducto)), []byte(datosProducto))
			if err != nil {
				return err
			}
		}
		fmt.Println("insertados productos en boltdb")
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}

func EscribirDireccionBOLT(){
	// Abrir la conexión a la base de datos SQL
	dbSQL, err := sql.Open("postgres", "user=postgres dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer dbSQL.Close()

	// Abrir la base de datos BoltDB en modo lectura y escritura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar consulta SQL para obtener datos de productos
	rows, err := dbSQL.Query("SELECT * FROM direccion_entrega")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Actualizar BoltDB con datos de productos
	err = dbBolt.Update(func(tx *bolt.Tx) error {
		// Crear o abrir el bucket "direccion"
		bucket, err := tx.CreateBucketIfNotExists([]byte("direccion"))
		if err != nil {
			return err
		}

		// Procesar resultados
		for rows.Next() {
			var id_usuarie int
			var id_direccion_entrega int
			var direccion,localidad,codigo_postal string

			// Escanear los valores desde la fila SQL
			err := rows.Scan(&id_usuarie, &id_direccion_entrega, &direccion, &localidad, &codigo_postal)
			if err != nil {
				log.Fatal(err)
			}

			// Formatear los datos según sea necesario antes de insertar en BoltDB
			datosDireccion := fmt.Sprintf("%v,%v,%v,%v,%v", id_usuarie, id_direccion_entrega, direccion, localidad, codigo_postal)

			// Insertar datos en el bucket usando el ID de la direccion como clave
			err = bucket.Put([]byte(fmt.Sprintf("%d", id_direccion_entrega)), []byte(datosDireccion))
			if err != nil {
				return err
			}
		}
		fmt.Println("insertadas direcciones en boltdb")
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}

func EscribirPedidosBOLT(){
	// Abrir la conexión a la base de datos SQL
	dbSQL, err := sql.Open("postgres", "user=postgres dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer dbSQL.Close()

	// Abrir la base de datos BoltDB en modo lectura y escritura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar consulta SQL para obtener datos de productos
	rows, err := dbSQL.Query("SELECT * FROM pedido")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	// Actualizar BoltDB con datos de pedidos
	err = dbBolt.Update(func(tx *bolt.Tx) error {
		// Crear o abrir el bucket "pedido"
		bucket, err := tx.CreateBucketIfNotExists([]byte("pedido"))
		if err != nil {
			return err
		}

		// Procesar resultados
		for rows.Next() {
			var id_pedido, id_usuarie, id_direccion_entrega int
			var f_pedido, estado string
			var monto_total, costo_envio float64 
			var fecha_entrega, hora_entrega_desde, hora_entrega_hasta sql.NullString //como no se entregan los pedidos mediante este sistema, no hay fecha de entrega
			//por lo que lo definimos como un string nulo 
			// Escanear los valores desde la fila SQL
			err := rows.Scan(&id_pedido, &f_pedido, &fecha_entrega, &hora_entrega_desde, &hora_entrega_hasta, &id_usuarie, &id_direccion_entrega, &monto_total, &costo_envio, &estado)
			if err != nil {
				log.Fatal(err)
			}

			// Formatear los datos según sea necesario antes de insertar en BoltDB
			datosPedido := fmt.Sprintf("%v,%v,%v,%v,%v,%v,%v,%v,%v,%v", id_pedido, f_pedido, fecha_entrega, hora_entrega_desde, hora_entrega_hasta, id_usuarie, id_direccion_entrega, monto_total, costo_envio, estado)

			// Insertar datos en el bucket usando el ID del pedido como clave
			err = bucket.Put([]byte(fmt.Sprintf("%d", id_pedido)), []byte(datosPedido))
			if err != nil {
				return err
			}
		}
		fmt.Println("insertados pedidos en boltdb")
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
}
//-----------------Lectura de buckets ------------------------------------
func LeerClientesBOLT() {
	// Abrir la base de datos BoltDB en modo solo lectura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0400, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar operación de lectura en la base de datos BoltDB
	err = dbBolt.View(func(tx *bolt.Tx) error {
		// Obtener el bucket "cliente"
		bucket := tx.Bucket([]byte("cliente"))
		if bucket == nil {
			return fmt.Errorf("Bucket 'cliente' no encontrado")
		}

		// Iterar sobre todas las claves y valores en el bucket
		err := bucket.ForEach(func(k, v []byte) error {
			// Convertir bytes a string
			clave := string(k)
			valor := string(v)

			// Imprimir la clave y el valor
			fmt.Printf("ClaveCliente: %s, ValorCliente: %s\n", clave, valor)

			return nil
		})
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func LeerProductosBOLT() {
	// Abrir la base de datos BoltDB en modo solo lectura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0400, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar operación de lectura en la base de datos BoltDB
	err = dbBolt.View(func(tx *bolt.Tx) error {
		// Obtener el bucket "producto"
		bucket := tx.Bucket([]byte("producto"))
		if bucket == nil {
			return fmt.Errorf("Bucket 'producto' no encontrado")
		}

		// Iterar sobre todas las claves y valores en el bucket
		err := bucket.ForEach(func(k, v []byte) error {
			// Convertir bytes a string
			clave := string(k)
			valor := string(v)

			// Imprimir la clave y el valor
			fmt.Printf("ClaveProd: %s, ValorProd: %s\n", clave, valor)

			return nil
		})
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
}

func LeerDireccionesBOLT() {
	// Abrir la base de datos BoltDB en modo solo lectura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0400, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar operación de lectura en la base de datos BoltDB
	err = dbBolt.View(func(tx *bolt.Tx) error {
		// Obtener el bucket "direccion"
		bucket := tx.Bucket([]byte("direccion"))
		if bucket == nil {
			return fmt.Errorf("Bucket 'direccion' no encontrado")
		}

		// Iterar sobre todas las claves y valores en el bucket
		err := bucket.ForEach(func(k, v []byte) error {
			// Convertir bytes a string
			clave := string(k)
			valor := string(v)

			// Imprimir la clave y el valor
			fmt.Printf("ClaveDirec: %s, ValorDirec: %s\n", clave, valor)

			return nil
		})
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
} 
func LeerPedidoBOLT() {
	// Abrir la base de datos BoltDB en modo solo lectura
	dbBolt, err := bolt.Open("albarracin_bravo_gonzalez_zapata_db1.db", 0400, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer dbBolt.Close()

	// Realizar operación de lectura en la base de datos BoltDB
	err = dbBolt.View(func(tx *bolt.Tx) error {
		// Obtener el bucket "pedido"
		bucket := tx.Bucket([]byte("pedido"))
		if bucket == nil {
			return fmt.Errorf("Bucket 'pedido' no encontrado")
		}

		// Iterar sobre todas las claves y valores en el bucket
		err := bucket.ForEach(func(k, v []byte) error {
			// Convertir bytes a string
			clave := string(k)
			valor := string(v)

			// Imprimir la clave y el valor
			fmt.Printf("ClavePedido: %s, ValorPedido: %s\n", clave, valor)

			return nil
		})
		return err
	})

	if err != nil {
		log.Fatal(err)
	}
} 
//----------llamada a las funciones de lectura y de escritura -----------
func CargarDatosBolt(){
	EscribirClientesBOLT();
	EscribirDireccionBOLT();
	EscribirProductosBOLT();
	EscribirPedidosBOLT();
	}
func LeerBuckets(){
	LeerClientesBOLT();
	LeerDireccionesBOLT();
	LeerProductosBOLT();
	LeerPedidoBOLT();
	}

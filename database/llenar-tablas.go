package database

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"os"
	"time"
)

// Define una estructura para deserializar el JSON
type Cliente struct {
	IdCliente       int    `json:"id_usuarie"`
	NombreCliente   string `json:"nombre"`
	ApellidoCliente string `json:"apellido"`
	DniCliente      int    `json:"dni"`
	FechaNac        string `json:"fecha_nacimiento"`
	TelefonoCliente string `json:"telefono"`
	EmailCliente    string `json:"email"`
}

type Direccion struct {
	IdCli     int    `json:"id_usuarie"` // IdUsuarie
	IdDir     int    `json:"id_direccion_entrega"`
	Dir       string `json:"direccion"`
	Localidad string `json:"localidad"`
	CodPostal string `json:"codigo_postal"`
}

type Tarifa struct {
	CodPostalCorto string  `json:"codigo_postal_corto"`
	Costo          float64 `json:"costo"`
}

type Producto struct {
	ID              int     `json:"id_producto"`
	Nombre          string  `json:"nombre"`
	PrecioUnitario  float64 `json:"precio_unitario"`
	StockDisponible int     `json:"stock_disponible"`
	StockReservado  int     `json:"stock_reservado"`
	PuntoReposicion int     `json:"punto_reposicion"`
	StockMaximo     int     `json:"stock_maximo"`
}

type EntradaTrxPedido struct {
	IdOrden            int    `json:"id_orden"`
	Operacion          string `json:"operacion"`
	IdUsuarie          int    `json:"id_usuarie"`
	IdDireccionEntrega int    `json:"id_direccion_entrega"`
	IdPedido           int    `json:"id_pedido"`
	IdProducto         int    `json:"id_producto"`
	Cantidad           int    `json:"cantidad"`
	FechaHoraEntrega   string `json:"fecha_hora_entrega"`
}

// CadenaConexion es la cadena necesaria para conectar a la base de datos
const conexionDB = "user=postgres host=localhost dbname=albarracin_bravo_gonzalez_zapata_db1 sslmode=disable"

func abrirConexion() (*sql.DB, error) {
	db, err := sql.Open("postgres", conexionDB)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func LlenarClientes(db *sql.DB) {
	log.Println("Iniciando llenado de datos de clientes...")

	//lee el archivo json
	jsonFile, err := os.Open("json/clientes.json")
	if err != nil {
		log.Fatalf("Error al abrir el archivo JSON de clientes: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatalf("Error al leer el contenido del archivo JSON de clientes: %v", err)
	}

	var clientes []Cliente
	// Deserializa el JSON en una estructura Go
	if err := json.Unmarshal(byteValue, &clientes); err != nil {
		log.Fatalf("Error al deserializar el JSON de clientes: %v", err)
	}

	// Recorre los productos y escribe los datos en la base de datos
	for _, cliente := range clientes {
		_, err := db.Exec(`
			INSERT INTO cliente (id_usuarie, nombre, apellido, dni, fecha_nacimiento, telefono, email)
			VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			cliente.IdCliente, cliente.NombreCliente, cliente.ApellidoCliente, cliente.DniCliente, cliente.FechaNac, cliente.TelefonoCliente, cliente.EmailCliente)
		if err != nil {
			log.Fatalf("Error al insertar cliente %d: %v", cliente.IdCliente, err)
		}
		/*else{
			log.Printf("Cliente %d insertado correctamente", cliente.IdCliente)
		}*/ //Al sacar este comentario puedes ver uno por uno que cliente a sido correctamente agregado
	}

	log.Println("Llenado de datos de clientes completado.")
}

func LlenarDirecciones(db *sql.DB) {
	log.Println("Iniciando llenado de datos de direcciones...")

	// Lee el archivo JSON
	jsonFile, err := os.Open("json/direcciones.json")
	if err != nil {
		log.Fatalf("Error al abrir el archivo JSON de direcciones: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatalf("Error al leer el contenido del archivo JSON de direcciones: %v", err)
	}

	var dir []Direccion

	// Deserializa el JSON en una estructura Go
	if err := json.Unmarshal(byteValue, &dir); err != nil {
		log.Fatalf("Error al deserializacion el JSON de direcciones: %v", err)
	}

	// Recorre los productos y escribe los datos en la base de datos
	for _, direccion := range dir {
		_, err := db.Exec(`
			INSERT INTO direccion_entrega (id_usuarie, id_direccion_entrega, direccion, localidad, codigo_postal)
			VALUES ($1, $2, $3, $4, $5)`,
			direccion.IdCli, direccion.IdDir, direccion.Dir, direccion.Localidad, direccion.CodPostal)
		if err != nil {
			log.Printf("Error al insertar direccion %d: %v", direccion.IdDir, err)
		} /*else{
			log.Printf("Direccion %d insertada correctamente", direccion.IdDir)
		}*/
	}

	log.Println("Llenado de datos de direcciones completado.")
}

func LlenarTarifas(db *sql.DB) {
	// Lee el archivo JSON
	jsonFile, err := os.Open("json/tarifas_entrega.json")
	if err != nil {
		log.Fatalf("Error al abrir el archivo JSON de tarifas de entrega: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatalf("Error al leer el contenido del el archivo JSON de tarifas de entrega: %v", err)
	}

	var tarifaEntregas []Tarifa

	// Deserializa el JSON en una estructura Go
	if err := json.Unmarshal(byteValue, &tarifaEntregas); err != nil {
		log.Fatalf("Error al deserializar el JSON de tarifas de entrega: %v", err)
	}

	// Recorre los productos y escribe los datos en la base de datos
	for _, tarifa := range tarifaEntregas {
		_, err := db.Exec(`
			INSERT INTO tarifa_entrega (codigo_postal_corto, costo)
			VALUES ($1, $2)`,
			tarifa.CodPostalCorto, tarifa.Costo)
		if err != nil {
			log.Printf("Error al insertar tarifa de entrega %s: %v", tarifa.CodPostalCorto, err)
		} /*else{
			log.Printf("Tarifa de entrega %s insertada correctamente", tarifa.CodPostalCorto)
		}*/
	}

	log.Println("Llenado de datos de tarifas de entrega completado.")
}

func LlenarProductos(db *sql.DB) {
	log.Println("Iniciando llenado de datos de productos...")

	// Lee el archivo JSON
	jsonFile, err := os.Open("json/productos.json")
	if err != nil {
		log.Fatalf("Error al abrir el archivo JSON de productos: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatalf("Error al leer el contenido del archivo JSON de productos: %v", err)
	}

	var productos []Producto

	// Deserializa el JSON en una estructura Go
	if err := json.Unmarshal(byteValue, &productos); err != nil {
		log.Fatal("Error al deserializar el JSON DE PRODUCTOS: %V", err)
	}

	// Recorre los productos y escribe los datos en la base de datos
	for _, producto := range productos {
		_, err := db.Exec(`
			INSERT INTO producto (id_producto, nombre, precio_unitario, stock_disponible, stock_reservado, punto_reposicion, stock_maximo)
			VALUES ($1, $2, $3, $4, $5, $6, $7)`,
			producto.ID, producto.Nombre, producto.PrecioUnitario, producto.StockDisponible, producto.StockReservado, producto.PuntoReposicion, producto.StockMaximo)
		if err != nil {
			log.Printf("Error al insertar producto %d: %v", producto.ID, err)
		} /*else {
			log.Printf("Producto %d insertado correctamente", producto.ID)
		}*/
	}

	log.Println("Llenado de datos de productos completados.")
}

func LlenarEntradaTrxPedido(db *sql.DB) {
	log.Println("Iniciando llenado de datos de entrada_trx_pedido...")

	//Leer el archivo JSON
	jsonFile, err := os.Open("json/trx_pedido.json")
	if err != nil {
		log.Fatalf("Error al abrir el archivo JSON de entrada_trx_pedido: %v", err)
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Fatalf("Error al leer el contenido del archivo JSON de entrada_trx_pedido: %v", err)
	}
	defer jsonFile.Close()

	var entradas []EntradaTrxPedido

	//Deserializa el JSON en un estructura Go
	if err := json.Unmarshal(byteValue, &entradas); err != nil {
		log.Fatalf("Error al deserializar el JSON de entrada_trx_pedido: %v", err)
	}

	//Recorre las entradas y escribe los datos en la base de datos
	for _, entrada := range entradas {
		//Parsea el string de fecha y hora en formato "2008-01-02 15:04"
		if entrada.FechaHoraEntrega != "" {
			fechaHora, err := time.Parse("2006-01-02 15:04", entrada.FechaHoraEntrega)
			if err != nil {
				log.Printf("Error al parsear la fecha y hora para entrada_trx_pedido con id_orden %d: %v", entrada.IdOrden, err)
			}

			_, err = db.Exec(`
			insert into entrada_trx_pedido (id_orden, operacion, id_usuarie, id_direccion_entrega, id_pedido, id_producto, cantidad, fecha_hora_entrega)
			values ($1, $2, $3, $4, $5, $6, $7, $8)`,
				entrada.IdOrden, entrada.Operacion, entrada.IdUsuarie, entrada.IdDireccionEntrega, entrada.IdPedido, entrada.IdProducto, entrada.Cantidad, fechaHora)
			if err != nil {
				log.Printf("Error al insertar entrada_trx_pedido con id_orden %d: %v", entrada.IdOrden, err)
			}
		} else {
			_, err = db.Exec(`
			insert into entrada_trx_pedido (id_orden, operacion, id_usuarie, id_direccion_entrega, id_pedido, id_producto, cantidad)
			values ($1, $2, $3, $4, $5, $6, $7)`,
				entrada.IdOrden, entrada.Operacion, entrada.IdUsuarie, entrada.IdDireccionEntrega, entrada.IdPedido, entrada.IdProducto, entrada.Cantidad)
			if err != nil {
				log.Printf("Error al insertar entrada_trx_pedido con id_orden %d: %v", entrada.IdOrden, err)
			}
		}

	}
	log.Println("Llenado de datos de entrada_trx_pedido completado.")
}

func Completartablas() {
	db, err := abrirConexion()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	LlenarClientes(db)
	fmt.Println("Datos de clientes insertados en la base de datos.")
	LlenarDirecciones(db)
	fmt.Println("Datos de las direcciones insertados en la base de datos.")
	LlenarTarifas(db)
	fmt.Println("Datos de las tarifas de entrega insertados en la base de datos")
	LlenarProductos(db)
	fmt.Println("Datos de productos insertados en la base de datos.")
	LlenarEntradaTrxPedido(db)
	fmt.Println("Datos de Entrada de pedidos insertados en la base de datos.")

}

// no se olviden de hacer el gofmt -w
package main

import (
	"fmt"
	_ "github.com/lib/pq"
	"os"
	"time"
	database "tp-2023-db1/database"
)

// PresentarMenu muestra el menu en la consola
func PresentarMenu() {
	fmt.Printf(`Seleccione: 
	0 Crear DB
	1 Crear tablas
	2 Agregar PK y FK
	3 Borrar PK y FK
	4 Cargar datos
	5 Crear SP y TRG
	6 Procesar pedidos automatico
	7 Utilizar funciones manualmente
	8 Cargar datos en BoltDB
	9 Leer datos cargados en BoltDB
	10 Salir
	  
	  `)
}

// EjecutarOpcion ejecuta la logica correspondiente a la opcion seleccionada
func EjecutarOpcion(opcion int) {
	switch opcion {
	case 0:
		fmt.Println("Realizando la operacion: CrearDB")
		database.Crear()
	case 1:
		fmt.Println("Realizando la operacion: Crear tablas")
		database.CrearTablas()
	case 2:
		fmt.Println("Realizando la operacion: Agregar Pk y Fk")
		database.AsignarPKFK()
	case 3:
		fmt.Println("Realizando la operacion: Borrar Pk y Fk")
		database.BorrarPKFK()
	case 4:
		fmt.Println("Realizando la operacion: Cargar datos")
		database.Completartablas()
	case 5:
		fmt.Println("Realizando la operacion: Agregar funciones")
		database.AgregarFunciones()
	case 6:
		fmt.Println("Realizando la operacion: Procesar pedidos")
		database.Procesar()
	case 7:
		fmt.Println("Ingresando al menu de funciones")
		database.FuncionesManual()
	case 8:
		fmt.Println("Escribiendo datos en BoltDB")
		database.CargarDatosBolt()
	case 9:
		fmt.Println("Leyendo buckets de BoltDB")
		database.LeerBuckets()
	case 10:
		fmt.Println("Saliendo del programa.")
		os.Exit(0) //se cambio el break y return ya que estos no terminaban el programa
	default:
		fmt.Println("opción inválida")
	}
}

func main() {
	var input int
	var sn string
	fin := true

	for fin {
		PresentarMenu()

		fmt.Printf("ingrese un número: ")
		fmt.Scanf("%d", &input) //El ingreso de letras o algo distinto a un numero produce que se repita la ultima opcion ingresada

		EjecutarOpcion(input)

		fmt.Println("Desea ejecutar otra operacion? s/n: ")
		fmt.Scanf("%s", &sn)
		if sn == "n" {
			break
		}

	}

	fmt.Println("Saliendo del programa.")
	time.Sleep(1 * time.Second)
}
